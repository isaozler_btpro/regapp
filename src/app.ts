import * as express from "express";
import * as bodyParser from "body-parser";

const app = express();

app.set("port", process.env.IISNODE_VERSION || 3768);
app.use((req: any, res: any, next: any) => {
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Username, Password, Authorization");
	res.header("Access-Control-Allow-Methods", "DELETE, GET, HEAD, POST, PUT, OPTIONS, TRACE");
	res.header("Access-Control-Allow-Origin", "*");
	res.header("x-Reference", "BTPro contact@btpro.net");
	res.header("x-Powered-By", "BTServer");
	res.header("Server", "BTServer");
	next();
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('*', (req: any, res: any) => {
	res.status(200).json({ message: 'hello io' });
});